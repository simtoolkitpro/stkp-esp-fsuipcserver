﻿using FSUIPC;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Timers;
using System.Windows.Forms;

namespace FSUIPCServer
{
    public partial class FSUIPCServer : Form
    {

        #region FSUIPC Offsets
        Offset<short> FS_ONGROUND = new Offset<short>(0x0366);
        Offset<int> FS_SIMPAUSE_STATE = new Offset<int>(0x0264);
        Offset<string> FS_METAR = new Offset<string>(0xB800, 2000);
        Offset<string> FS_AC_NAME = new Offset<string>(0x3D00, 256);

        Offset<byte> FS_TIME_H = new Offset<byte>(0x0238);
        Offset<byte> FS_TIME_M = new Offset<byte>(0x0239);
        Offset<byte> FS_ZTIME_H = new Offset<byte>(0x023B);
        Offset<byte> FS_ZTIME_M = new Offset<byte>(0x023C);
        Offset<byte> FS_TIME_D = new Offset<byte>(0x0245);
        Offset<byte> FS_TIME_MO = new Offset<byte>(0x0244);
        // Positional
        Offset<long> FS_LOC_LAT = new Offset<long>(0x0560);
        Offset<long> FS_LOC_LON = new Offset<long>(0x0568);
        Offset<long> FS_LOC_ALT = new Offset<long>(0x0570);
        Offset<long> FS_LOC_HEAD = new Offset<long>(0x0580);
        Offset<int> FS_LOC_GDS = new Offset<int>(0x02B4);
        Offset<int> FS_LOC_IAS = new Offset<int>(0x02BC);
        Offset<int> FS_LOC_TAS = new Offset<int>(0x02B8);
        Offset<short> FS_GFORCE = new Offset<short>(0x11BA);
        readonly Offset<short> FS_GROUNDALT = new Offset<short>(0x0B4C);

        Offset<int> FS_AIR = new Offset<int>(0x055C);

        Offset<int> FS_LOC_PITCH = new Offset<int>(0x0578);
        Offset<int> FS_LOC_ROLL = new Offset<int>(0x057C);
        Offset<short> FS_LOC_VSPD = new Offset<short>(0x02C8);

        Offset<short> FS_VSPD_NOUPD = new Offset<short>(0x030C);

        Offset<int> FS_FUEL_WEIGHT = new Offset<int>(0x126C);
        Offset<double> FS_E1_RPM = new Offset<double>(0x2018);
        Offset<double> FS_E2_RPM = new Offset<double>(0x2118);
        Offset<double> FS_E3_RPM = new Offset<double>(0x2218);
        Offset<double> FS_E4_RPM = new Offset<double>(0x2318);

        CultureInfo Eng = CultureInfo.GetCultureInfo("en-GB");

        #endregion

        System.Timers.Timer locationPulse;

        public FSUIPCServer()
        {
            InitializeComponent();

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
        }

        void FSUIPCProcess()
        {
            try
            {
                FSUIPCConnection.Process();
            } catch (FSUIPC.FSUIPCException ex)
            {
                Debug.WriteLine(ex.Message);
                Console.Write("dc");
                Debug.WriteLine("dc sent");
                Console.Out.Flush();
                if (FSUIPCConnection.IsOpen)
                {
                    FSUIPCConnection.Close();
                }
            }
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Console.Write("dc");
            Console.Out.Flush();
            if (FSUIPCConnection.IsOpen)
            {
                FSUIPCConnection.Close();
            }

        }

        private void FSUIPCServer_Load(object sender, EventArgs e)
        {
            // Get a connection 
            FSUIPCConnect();

            if (!FSUIPCConnection.IsOpen)
            {
                Debug.WriteLine("No Connection");
            } else
            {
                Debug.WriteLine("Connected");
            }

            locationPulse = new System.Timers.Timer();
            locationPulse.Elapsed += new ElapsedEventHandler(LocationPulse);
            locationPulse.Interval = 100;
            locationPulse.Enabled = true;

            var input = Console.OpenStandardInput();

            var buffer = new byte[1024];
            int length;
            while (input.CanRead && (length = input.Read(buffer, 0, buffer.Length)) > 0)
            {

                string payload = ""; 

                string _input = Encoding.UTF8.GetString(buffer);

                if (!FSUIPCConnection.IsOpen)
                {
                    payload = "NoConnection";
                    Console.Write(payload);
                    Console.Out.Flush();
                    FSUIPCConnect();
                    continue;
                } else
                {
                    FSUIPCProcess();
                }

                string[] cmd = _input.TrimEnd(new char[] { (char)0 }).Trim().Split(' ');
                Debug.WriteLine(cmd.Length);
                Debug.WriteLine(cmd[0]);

                switch (cmd[0])
                {
                    case "Connect":
                        Debug.WriteLine("Returning Connected");
                        payload = "Connected";
                        break;
                    case "Version":
                        payload = $"Connected to simulator {FSUIPCConnection.FlightSimVersionConnected.ToString()}";
                        break;
                    case "Location":
                        payload = CreateDataPackage();
                        break;
                    case "PauseState":
                        payload = CreateStatePackage();
                        break;
                }

                Console.Write(payload);
                Console.Out.Flush();
                continue;
            }
        }

        private void LocationPulse(object sender, ElapsedEventArgs e)
        {
            if (FSUIPCConnection.IsOpen)
            {
                locationPulse.Interval = 500;
                string payload = CreateDataPackage();
                Console.Write(payload);
                Debug.WriteLine(payload);
                Console.Out.Flush();
            } else
            {
                locationPulse.Interval = 5000;
                FSUIPCConnect();
            }
        }

        void FSUIPCConnect()
        {
            Debug.WriteLine("connecting");
            try
            {
                FSUIPCConnection.Open();
                Debug.WriteLine("connected");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private string CreateStatePackage()
        {
            return $"pau {(FS_SIMPAUSE_STATE.Value == 1 ? "true" : "false")}";
        }

        private string CreateDataPackage()
        {
            
            FSUIPCProcess();

            bool engon = FS_E1_RPM.Value > 5 || FS_E2_RPM.Value > 5 || FS_E3_RPM.Value > 5 || FS_E4_RPM.Value > 5;

            dynamic[] parts = new dynamic[]
            {
                (FS_LOC_LAT.Value * (90D / (10001750D * 65536D * 65536D))).ToString(Eng),                               
                (FS_LOC_LON.Value * (360D / (65536D * 65536D * 65536D * 65536D))).ToString(Eng),                        
                (Math.Ceiling(FS_LOC_ALT.Value * 3.28084 / (65536L * 65536L))).ToString(Eng),
                ((float)((uint)FS_LOC_HEAD.Value * 360d / (65536L * 65536L))).ToString(Eng),
                (Math.Floor(FS_LOC_IAS.Value / 128d)).ToString(Eng),
                (Math.Round((double)FS_LOC_PITCH.Value * (360D / (65536D * 65536D)), 2)).ToString(Eng),
                (Math.Round((double)FS_LOC_ROLL.Value * (360D / (65536D * 65536D)), 2)).ToString(Eng),
                (FS_LOC_VSPD.Value * (60D * (3.28084 / 256D))).ToString(Eng),
                (FS_VSPD_NOUPD.Value * (60D * (3.28084 / 256D))).ToString(Eng),
                (Math.Round((double)(FS_GFORCE.Value / 625), 2)).ToString(Eng),
                (Math.Floor((double)(FS_LOC_ALT.Value / (65536L * 65536L)) - FS_GROUNDALT.Value)).ToString(Eng),
                FS_ONGROUND.Value.ToString(Eng),
                "0",
                FS_AC_NAME.Value,
                FS_METAR.Value,
                (Math.Floor(FS_LOC_TAS.Value / 128d)).ToString(Eng),
                (Math.Floor((FS_LOC_GDS.Value / 65536d) * 1.94384)).ToString(Eng),
                (Math.Ceiling((double)(FS_LOC_ALT.Value / (65536L * 65536L)))).ToString(Eng),
                string.Join("", FS_TIME_H.Value.ToString().PadLeft(2, '0'), FS_TIME_M.Value.ToString().PadLeft(2, '0')),
                engon ? "1" : "0",
                string.Join("", FS_ZTIME_H.Value.ToString().PadLeft(2, '0'), FS_ZTIME_M.Value.ToString().PadLeft(2, '0')),
                Math.Floor(FS_FUEL_WEIGHT.Value / 2.205).ToString()
            };

            return $"____loc {string.Join("#", parts)}";


            //FS_FUEL_WEIGHT, 

            // lat, lon, alt, head, speed, pitch, roll, vspd, vspd2, gforce, rel_alt, ac, metar, simtime(H/M), simtime(D/M), simzulutime(H/M)

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
